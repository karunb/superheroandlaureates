//
//  laureatesmodelclass.swift
//  SuperheroesandLaureates
//
//  Created by Bourishetty,Karun on 4/12/19.
//  Copyright © 2019 Bourishetty,Karun. All rights reserved.
//

import Foundation

struct Laureate:Codable {
    let firstname:String?
    let surname:String?
    let born:String?
    let died:String?
}
