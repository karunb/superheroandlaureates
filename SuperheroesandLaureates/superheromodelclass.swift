//
//  superheromodelclass.swift
//  SuperheroesandLaureates
//
//  Created by Bourishetty,Karun on 4/12/19.
//  Copyright © 2019 Bourishetty,Karun. All rights reserved.
//

import Foundation

struct Squad:Codable {
    let members:[Superhero]
}

struct Superhero:Codable {
    let name:String
    let secretIdentity:String
    let powers:[String]
}
