//
//  FirstViewController.swift
//  SuperheroesandLaureates
//
//  Created by Bourishetty,Karun on 4/12/19.
//  Copyright © 2019 Bourishetty,Karun. All rights reserved.
//

import UIKit

class superherosTVC: UITableViewController {

    var superHeroData:[Superhero] = []
    
    // Load superheros from the specified json file, if they have not been loaded already
    override func viewWillAppear(_ animated: Bool) {
        if superHeroData.count == 0 {
            URLSession.shared.dataTask(with: URL(string: "https://www.dropbox.com/s/wpz5yu54yko6e9j/squad.json?dl=1")!, completionHandler: postingSuperheroData).resume()
        }
    }
    
    // Set hero data to the array of heros, reload tableview
    func postingSuperheroData(data:Data?, urlResponse:URLResponse?, error:Error?) {
        superHeroData = try! JSONDecoder().decode(Squad.self, from: data!).members
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    // Configure tableview cells
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "hero", for: indexPath)
        cell.textLabel?.text = "\(superHeroData[indexPath.row].name) (AKA: \(superHeroData[indexPath.row].secretIdentity))"
        cell.detailTextLabel?.text = "\(superHeroData[indexPath.row].powers.joined(separator: ", "))"
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView,
                            titleForHeaderInSection section: Int) -> String?{
        if section == 0{
            return "SUPERHEROES"
        }
        else{
            return "NULL"
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return superHeroData.count
    }


}

