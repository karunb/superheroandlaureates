    //
//  SecondViewController.swift
//  SuperheroesandLaureates
//
//  Created by Bourishetty,Karun on 4/12/19.
//  Copyright © 2019 Bourishetty,Karun. All rights reserved.
//

import UIKit

class LaureatesTVC: UITableViewController {
    var laureatesData:[Laureate] = []
    
    // Load laureates from the specified json file, if they have not been loaded already
    override func viewWillAppear(_ animated: Bool) {
        if laureatesData.count == 0 {
            URLSession.shared.dataTask(with: URL(string: "https://www.dropbox.com/s/7dhdrygnd4khgj2/laureates.json?dl=1")!, completionHandler: postingLaureateData).resume()
        }
    }
    
    // Set laureate data to the array of laureates, reload tableview
    func postingLaureateData(data:Data?, urlResponse:URLResponse?, error:Error?) {
        /*laureatesData = try! JSONDecoder().decode([Laureate].self, from: data!)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }*/
        var name:[[String:Any]]!
        var firstname:String?
        var lastname:String?
        var born:String?
        var died:String?
        do {
            try name = JSONSerialization.jsonObject(with: data!,
                                                    options: .allowFragments) as? [[String:Any]]
            for i in name {
                firstname = i["firstname"] as? String // not [String:Double]
                lastname = i["surname"] as? String
                born  = i["born"] as? String
                died = i["died"] as? String
                //display(message:String(format:"%.1f °F", temperature))
                let obkct  = Laureate(firstname: firstname, surname: lastname, born: born, died: died)
                laureatesData.append(obkct)
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }catch {
            print(error)
        }
    }
    
    // Configure tableview cells
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "laureates", for: indexPath)
        
        let nameLBL = cell.viewWithTag(1) as! UILabel
        let dobLBL = cell.viewWithTag(2) as! UILabel
        
        nameLBL.text = "\(laureatesData[indexPath.row].firstname ?? "No FirstName") \(laureatesData[indexPath.row].surname ?? "No SurName")"
        dobLBL.text = laureatesData[indexPath.row].born == "0000-00-00" ? "null" : laureatesData[indexPath.row].died == "0000-00-00" ? "\(laureatesData[indexPath.row].born ?? "nil")  to  Present" : "\(laureatesData[indexPath.row].born ?? "nil")  to  \(laureatesData[indexPath.row].died ?? "nil")"
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> String {
  //      return "LAUREATES"
   // }
    
    override func tableView(_ tableView: UITableView,
                            titleForHeaderInSection section: Int) -> String?{
        if section == 0{
            return "LAUREATES"
        }
        else{
            return "NULL"
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return laureatesData.count
    }

}

